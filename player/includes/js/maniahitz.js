function mutar(){
  mute = document.getElementById('audio'); 
  if( mute.muted ){
    mute.muted = false;
  }else{
    mute.muted = true;
  }
}
effect = {
  init: function(){
    var speaker = $('#locutor'),
        program = $('#programa'),
        listeners = $('#ouvintes'),
		imgloc = $('#imgloc');
    speaker.animate({'opacity':'0.3'}, 500);
    speaker.animate({'opacity':'0.8'}, 500);
    program.animate({'opacity':'0.3'}, 500);
    program.animate({'opacity':'0.8'}, 500);
    listeners.animate({'opacity':'0.3'}, 500);
    listeners.animate({'opacity':'0.8'}, 500);
	imgloc.animate({'opacity':'0.3'}, 500);
    imgloc.animate({'opacity':'0.8'}, 500);
    speaker.animate({'opacity':'0.3'}, 500);
    speaker.animate({'opacity':'0.8'}, 500);
    program.animate({'opacity':'0.3'}, 500);
    program.animate({'opacity':'0.8'}, 500);
    listeners.animate({'opacity':'0.3'}, 500);
    listeners.animate({'opacity':'0.8'}, 500);
	imgloc.animate({'opacity':'0.3'}, 500);
    imgloc.animate({'opacity':'0.8'}, 500);
  },
  finish: function(){
    var speaker = $('#locutor'),
        program = $('#programa'),
        listeners = $('#ouvintes'),
		imgloc = $('#imgloc');
    speaker.animate({'opacity':'1'}, 500);
    program.animate({'opacity':'1'}, 500);
    listeners.animate({'opacity':'1'}, 500);
	imgloc.animate({'opacity':'1'}, 500);
  }
}

  $("#reload-btn").click(function(){
    site.status(true);
  });
  $("#player").draggable({axis:"x",containment:"#area_player",scroll:false,handle:".handle"});
 
  $(".o-c").click(function(){
    $("#player").toggleClass("minimize");
  });
  $(".playerpe").click(function(){
    $(".faplay").toggleClass("fa-play");
	$(".faplay").toggleClass("fa-pause"); 
  });
  // PLAYER CONTROLER
    var Player = {
      toggleP:function(){
        if ($('#audio').hasClass('pause') === true){
          $("#demo").html();
          $("#demo").html('<audio id="audio" src="http://167.114.53.24:9984/" ></audio>');
          var currentVolume = ($('#volumeHidden').html())/100;
          $("#audio").prop("volume", currentVolume).trigger('play');

        }else{
            $("#audio").trigger("pause");
            $("#audio").addClass('pause');
        }
      }

    }
  // FIM PLAYER CONTROLER

  //ALERT CONTROLER

    var Alert = {
      show: function(time, html, minimeizeDiv, minimeizeClass){
          $("#alertContent").html(html);
          $("#player-alert").removeClass('minimize-alert');
          setTimeout(function(){ 
            $("#alertContent").html("");
            $("#player-alert").addClass('minimize-alert');
            $(minimeizeDiv).addClass(minimeizeClass);
            $(".player").fadeIn('fast');
        }, time);
        
      }
    }

  //FIM ALERT CONTROLLER
  $(function(){
  // PEDIDOS
    // $("#pedidos-btn").click(function() {
    //   $("#player-pedidos").removeClass('minimize-pedidos');
    //   $("#pedido-submit").prop("disabled",false).html("Enviar");
    //   $("#Pedido").val("");
    //   $(".player").fadeOut('fast');
    // });
    // $("#pedidos-form").submit(function() {
    //   $("#pedido-submit").prop("disabled",true).html("Carregando...");
    //   $.ajax({
    //     type: 'post',
    //     url: 'player/php/all.php?func=pedir',
    //     data: {
    //       'usuario': $("#nick-pedido").val(),
    //       'pedido':$("#pedido").val(),
    //       'locutor': $("#locutor").html()
    //     },
    //     success: function(data){
    //       if(data == 1){
    //         Alert.show("3000","<b>Aguarde 5 min para o proximo pedido</b>","#player-pedidos","minimize-pedidos");
    //       }else if(data == 2){
    //         Alert.show("3000","<b>Pedido enviado com sucesso!</b>","#player-pedidos","minimize-pedidos");
    //       }else{
    //         Alert.show("3000","<b>Erro Interno</b>","#player-pedidos","minimize-pedidos");
    //       }
    //     }
    //   });
    //   return false;
    // });
    // $("#pedidos-voltar").click(function(){
    //   $("#player-pedidos").toggleClass("minimize-pedidos");
    //   $(".player").fadeIn('fast');
    // });

  // FIM PEDIDOS
  // PRESENCA
  $("#presenca-btn").click(function() {
    $("#player-presenca").removeClass('minimize-presenca');
    $("#presenca-submit").prop("disabled",false).html("Enviar");
    $("#codigo").val("");
    $(".player").fadeOut('fast');
  });


  $("#presenca-form").submit(function() {
    $("#presenca-submit").prop("disabled",true).html("Carregando...");
    $.ajax({
      type: 'post',
      url: 'player/php/all.php?func=presenca',
      data: {
        'usuario': $("#nick-presenca").val(),
        'codigo':$('#codigo').val()
        },
        success: function(data){
          if(data == 1){
            Alert.show("3000","<b>Voce ja marcou presenca!</b>","#player-presenca","minimize-presenca");
          }else if(data == 2){
            Alert.show("5000","<b>Presenca Marcada com sucesso!</b>","#player-presenca","minimize-presenca");
          }else if(data == 3){
            Alert.show("3000","<b>Este codigo ja expirou!</b>","#player-presenca","minimize-presenca");
          }else{
            Alert.show("3000","<b>Erro Interno</b>","#player-presenca","minimize-presenca");
          }
        }
      });
      return false;
    });
  $("#presenca-voltar").click(function(){
    $("#player-presenca").toggleClass("minimize-presenca");
    $(".player").fadeIn('fast');
  });
// FIM PRESENÃ‡A
//VOLUME
 $("#volume > #barra").slider({
  min: 0,
  max: 100,
  value: 30,
  range: "min",
  animate: true,
  slide: function(event, ui) {
     var currentVolume = (ui.value)/100;
     $("#volumeHidden").html(ui.value);
     $("#audio").prop("volume", currentVolume);
  }
 });
});