$(document).ready(function () {
    $('#do-login').on('submit', function (e) {
        e.preventDefault();

        $('#login-name').attr("disabled", "true");
        $('#login-password').attr("disabled", "true");
        $('#btn-login').attr("disabled", "true");

        var username = $('#login-name').val();
        var password = $('#login-password').val();

        if (username == "" || password == "") {
            $("#alert-error").fadeIn("slow").removeClass("display-none").html('Nenhum campo pode estar em branco.');
            $('#login-name').removeAttr('disabled');
            $('#login-password').removeAttr('disabled');
            $('#btn-login').removeAttr('disabled');
            return;
        }

        $.ajax({
            type: "POST",
            url: "/home/login",
            data: {action: "login", username: username, password: password, remember: false},
            success: function (data) {
                var result = jQuery.parseJSON(data);
                if (result.valid == "true") {
                    $("#alert-error").addClass("display-none");
                    $("#alert-success").fadeIn("slow").removeClass("display-none").html('Login efetuado com sucesso! Você será redirecionado em alguns instantes.');
                    window.setTimeout(function () {
                        window.location = "";
                    }, 2000);
                } else {
                    $("#alert-error").fadeIn("slow").removeClass("display-none").html('Usuário ou senha inválidos, tente novamente!');
                    $('#login-name').removeAttr('disabled');
                    $('#login-password').removeAttr('disabled');
                    $('#btn-login').removeAttr('disabled');
                }
            }
        });
    });

    $('#alterar-senha').on('submit', function (e) {
        e.preventDefault();

        $('#regfield-senhaant').attr("disabled", "true");
        $('#regfield-senha').attr("disabled", "true");
        $('#regfield-senha-repeat').attr("disabled", "true");
        $('#btn-registrar').attr("disabled", "true");

        var senhantiga = $('#regfield-senhaant').val();
        var password = $('#regfield-senha').val();
        var passwordrepeat = $('#regfield-senha-repeat').val();

        if (senhantiga == "" || password == "" || passwordrepeat == "") {
            $("#alert-error").fadeIn("slow").removeClass("display-none").html('Nenhum campo pode estar em branco.');
            $('#regfield-senhaant').removeAttr('disabled');
            $('#regfield-senha').removeAttr('disabled');
            $('#regfield-senha-repeat').removeAttr('disabled');
            $('#btn-registrar').removeAttr('disabled');
            return;
        }

        $.ajax({
            type: "POST",
            url: "/configuracoes/mudarsenha",
            data: {action: "mudarsenha", senhantiga: senhantiga, password: password},
            success: function (data) {
                var result = jQuery.parseJSON(data);
                if (result.valid == "true") {
                    $("#alert-error").addClass("display-none");
                    $("#alert-success").fadeIn("slow").removeClass("display-none").html('Senha alterada com sucesso!');
                    window.setTimeout(function () {
                        window.location = "";
                    }, 5000);
                } else {
                    $("#alert-error").fadeIn("slow").removeClass("display-none").html('Ops, a senha antiga não confere!');
                    $('#regfield-senhaant').removeAttr('disabled');
                    $('#regfield-senha').removeAttr('disabled');
                    $('#regfield-senha-repeat').removeAttr('disabled');
                    $('#btn-registrar').removeAttr('disabled');
                }
            }
        });
    });
});

function check_regerrors() {
    $('#btn-registrar').attr("disabled", "true");
    var username = $('#regfield-username').val();
    var email = $('#regfield-email').val();
    var senha = $('#regfield-senha').val();
    var senhaequal = $('#regfield-senha-repeat').val();

    if(($("#user-invalid").hasClass("display-none").toString() == "false" ||
        $("#email-invalid").hasClass("display-none").toString() == "false" ||
        $("#senha-invalid").hasClass("display-none").toString() == "false" ||
        $("#senha-equal").hasClass("display-none").toString() == "false") ||
    (username == "" || email == "" || senha == "" || senhaequal == "") || grecaptcha.getResponse() == "")
    {
        $('#btn-registrar').attr("disabled", "true");
    } else {
        $('#btn-registrar').removeAttr('disabled');
    }
}

function check_regerrors_config() {
    $('#btn-registrar').attr("disabled", "true");
    var senhantiga = $('#regfield-senhaant').val();
    var password = $('#regfield-senha').val();
    var passwordrepeat = $('#regfield-senha-repeat').val();

    if(($("#senha-invalidantig").hasClass("display-none").toString() == "false" ||
            $("#senha-invalid").hasClass("display-none").toString() == "false" ||
            $("#senha-equal").hasClass("display-none").toString() == "false") ||
        (senhantiga == "" || password == "" || passwordrepeat == ""))
    {
        $('#btn-registrar').attr("disabled", "true");
    } else {
        $('#btn-registrar').removeAttr('disabled');
    }
}

function check_equals_config() {
    if($('#regfield-senha-repeat').val() != $('#regfield-senha').val()) {
        $("#reg-senha-repeat").addClass('input-group-danger');
        $("#reg-senha-repeat").removeClass('input-group-success');
        $("#senha-equal").removeClass('display-none');
        check_regerrors_config()
    } else {
        $("#reg-senha-repeat").removeClass('input-group-danger');
        $("#reg-senha-repeat").addClass('input-group-success');
        $("#senha-equal").addClass('display-none');
        check_regerrors_config()
    }
    check_regerrors_config();
}

function check_password_config(password) {
    $.ajax({
        type: "POST",
        url: "/registro/checkpassword",
        data: {action: "checkpassword", password: password},
        success: function (data) {
            var result = jQuery.parseJSON(data);
            if (result.valid == "true") {
                $("#reg-senha").removeClass('input-group-danger');
                $("#reg-senha").addClass('input-group-success');
                $("#senha-invalid").addClass('display-none');
                check_regerrors_config()
            } else {
                $("#reg-senha").addClass('input-group-danger');
                $("#reg-senha").removeClass('input-group-success');
                $("#senha-invalid").removeClass('display-none');
                check_regerrors_config()
            }
        }
    });
    check_regerrors_config();
}

function check_passanti(password) {
    if (password.length > 5) {
        $.ajax({
            type: "POST",
            url: "/configuracoes/senhaantiga",
            data: {action: "checkname", password: password},
            dataType: 'text',
            success: function (data) {
                var result = jQuery.parseJSON(data);
                if (result.valid == "true") {
                    $("#senha-antiga").removeClass('input-group-danger');
                    $("#senha-antiga").addClass('input-group-success');
                    $("#senha-invalidantig").addClass('display-none');
                    check_regerrors_config()
                } else {
                    $("#senha-antiga").addClass('input-group-danger');
                    $("#senha-antiga").removeClass('input-group-success');
                    $("#senha-invalidantig").removeClass('display-none');
                    check_regerrors_config()
                }
            }
        });
    } else {
        $("#senha-antiga").addClass('input-group-danger');
        $("#senha-antiga").removeClass('input-group-success');
        $("#senha-invalidantig").removeClass('display-none');
        check_regerrors_config()
    }
    check_regerrors_config();
}

function check_usernameerror() {
    $('#btn-registrar').attr("disabled", "true");
    var username = $('#regfield-username').val();

    if(($("#user-invalid").hasClass("display-none").toString() == "false") ||
        (username == ""))
    {
        $('#btn-registrar').attr("disabled", "true");
    } else {
        $('#btn-registrar').removeAttr('disabled');
    }
}

function check_email(email) {
    if (email.length > 4) {
        $.ajax({
            type: "POST",
            url: "/registro/checkemail",
            data: {action: "checkmail", email: email},
			dataType: 'text',
            success: function (data) {
                var result = jQuery.parseJSON(data);
                if (result.valid == "true") {
                    $("#reg-email").removeClass('input-group-danger');
                    $("#reg-email").addClass('input-group-success');
                    $("#email-invalid").addClass('display-none');
                    check_regerrors()
                } else {
                    $("#reg-email").addClass('input-group-danger');
                    $("#reg-email").removeClass('input-group-success');
                    $("#email-invalid").removeClass('display-none');
                    check_regerrors()
                }
			}
        });
    } else {
        $("#reg-email").addClass('input-group-danger');
        $("#reg-email").removeClass('input-group-success');
        $("#email-invalid").removeClass('display-none');
        check_regerrors()
    }
    check_regerrors()
}

function check_username(username) {
    if (username.length > 2) {
        $.ajax({
            type: "POST",
            url: "/registro/checkname",
            data: {action: "checkname", username: username},
            dataType: 'text',
            success: function (data) {
                var result = jQuery.parseJSON(data);
                if (result.valid == "true") {
                    $("#reg-usuario").removeClass('input-group-danger');
                    $("#reg-usuario").addClass('input-group-success');
                    $("#user-invalid").addClass('display-none');
                    check_regerrors()
                } else {
                    $("#reg-usuario").addClass('input-group-danger');
                    $("#reg-usuario").removeClass('input-group-success');
                    $("#user-invalid").removeClass('display-none');
                    check_regerrors()
                }
            }
        });
    } else {
        $("#reg-usuario").addClass('input-group-danger');
        $("#reg-usuario").removeClass('input-group-success');
        $("#user-invalid").removeClass('display-none');
        check_regerrors()
    }
    check_regerrors();
}

function register_username(username) {
    if (username.length > 2) {
        $.ajax({
            type: "POST",
            url: "/registro/checkname",
            data: {action: "checkname", username: username},
            dataType: 'text',
            success: function (data) {
                var result = jQuery.parseJSON(data);
                if (result.valid == "true") {
                    $("#reg-usuario").removeClass('input-group-danger');
                    $("#reg-usuario").addClass('input-group-success');
                    $("#user-invalid").addClass('display-none');
                    check_usernameerror()
                } else {
                    $("#reg-usuario").addClass('input-group-danger');
                    $("#reg-usuario").removeClass('input-group-success');
                    $("#user-invalid").removeClass('display-none');
                    check_usernameerror()
                }
            }
        });
    } else {
        $("#reg-usuario").addClass('input-group-danger');
        $("#reg-usuario").removeClass('input-group-success');
        $("#user-invalid").removeClass('display-none');
        check_regerrors()
    }
    check_usernameerror();
}

function check_password(password) {
    $.ajax({
        type: "POST",
        url: "/registro/checkpassword",
        data: {action: "checkpassword", password: password},
        success: function (data) {
            var result = jQuery.parseJSON(data);
            if (result.valid == "true") {
                $("#reg-senha").removeClass('input-group-danger');
                $("#reg-senha").addClass('input-group-success');
                $("#senha-invalid").addClass('display-none');
                check_regerrors()
            } else {
                $("#reg-senha").addClass('input-group-danger');
                $("#reg-senha").removeClass('input-group-success');
                $("#senha-invalid").removeClass('display-none');
                check_regerrors()
            }
        }
    });
    check_regerrors();
}

function check_equals() {
    if($('#regfield-senha-repeat').val() != $('#regfield-senha').val()) {
        $("#reg-senha-repeat").addClass('input-group-danger');
        $("#reg-senha-repeat").removeClass('input-group-success');
        $("#senha-equal").removeClass('display-none');
        check_regerrors()
    } else {
        $("#reg-senha-repeat").removeClass('input-group-danger');
        $("#reg-senha-repeat").addClass('input-group-success');
        $("#senha-equal").addClass('display-none');
        check_regerrors()
    }
    check_regerrors();
}