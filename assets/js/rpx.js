$(document).ready(function() {
    window.fbAsyncInit = function() {
        FB.init({
            appId: '137900693612220',
            xfbml: true,
            version: 'v2.10'
        });
    }
});

function fb_login() {
    FB.login(function(response) {
        if (response.status === 'connected') {
            send_data();
        }
    }, { scope: 'public_profile, email' });
}

function send_data() {
    FB.api('/me', { locale: 'pt_BR', fields: 'name, email' }, function(response) {
        response['action'] = "login";

        $.ajax({
            type: "POST",
            url: "/rpx/login",
            data: response,
            success: function (data) {
                if (data == 200) {
                    $("#alert-error").addClass("display-none");
                    $('#login-name').attr("disabled", "true");
                    $('#login-password').attr("disabled", "true");
                    $('#btn-login').attr("disabled", "true");
                    $("#alert-success").fadeIn("slow").removeClass("display-none").html('Aguarde alguns segundos, estamos redirecionando você...');
                    window.setTimeout(function () {
                        window.location = "/hotel";
                    }, 2000);
                } else if (data == 201) {
                    $("#alert-error").addClass("display-none");
                    $('#login-name').attr("disabled", "true");
                    $('#login-password').attr("disabled", "true");
                    $('#btn-login').attr("disabled", "true");
                    $("#alert-success").fadeIn("slow").removeClass("display-none").html('Aguarde alguns segundos, estamos redirecionando você...');
                    window.setTimeout(function () {
                        window.location = "";
                    }, 2000);
                } else {
                    $("#alert-success").addClass("display-none");
                    $('#login-name').removeAttr('disabled');
                    $('#login-password').removeAttr('disabled');
                    $('#btn-login').removeAttr('disabled');
                    $("#alert-error").fadeIn("slow").removeClass("display-none").html('Ocorreu um erro ao fazer login com facebook!');
                }
            }
        });
    });
}

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/pt_BR/all.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));