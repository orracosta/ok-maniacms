<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Limpador de URL para fins de SEO
function limpar($string){
    $table = array(
        '/'=>'', '('=>'', ')'=>'',
    );
    // Traduz os caracteres em $string, baseado no vetor $table
    $string = strtr($string, $table);
    $string= preg_replace('/[,.;:`´^~\'"]/', null, $string);
    $string= strtolower($string);
    $string= str_replace(" ", "-", $string);
    $string= str_replace("---", "-", $string);
    $string= str_replace("@", "a", $string);
    $string= str_replace("!", "", $string);
    $string= str_replace("?", "", $string);
    return $string;
}

function criarsso($player_id){
    return $ticket  = 'ON'. $player_id .'MANIA'. rand(1000,9999) .'SSO'. rand(1000,9999) . '-2017';
}

//Verifica requisitos minímos de nome de usuário
function fc_checkusername($username) {
    if(! preg_match("/^([a-zA-Z0-9-.:_]+)$/", $username) || strpos($username, 'MOD_') !== false || strpos($username, 'ADM_') !== false || strlen($username) < 3 || strlen($username) > 15)
        return true;
    else
        return false;
}

//Verifica requisitos minímos de email
function fc_checkemail($email){
    if(! preg_match("/^[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\-]+\.[a-z]{2,4}$/i", $email) OR strlen($email) < 7)
        return true;
    else
        return false;
}

//Verifica requisitos minímos de senha
function fc_checkpass($pass){
    if(strlen($pass) < 6)
        return true;
    else
        return false;
}

//Verifica se foi recebido em JSON
function jreturn($response, $json){
    //Retorna em formato JSON
    if ($json == 1)
        echo json_encode($response);
    //Retorna em formato PHP
    else
        return $response;
}

//Verifica se variaveis são iguais
function fc_checkequal($first, $second){
    if($first != $second)
        return false;
    else
        return true;
}

//Verifica se todas variaveis sao true
function fc_checkalltrue($array){
    $result = true;
    foreach ($array as $value => $valid){
        if($valid["valid"] == "false")
            $result = false;
    }
    return $result;
}

//Encripta senha
function fc_criptpass($passwd){
    $salt = '25ecgA2R34np52ab';
    return md5($salt.base64_encode($passwd));
}

//Caso tenha a variavel do cloudflare, utiliza ela para o IP
function cloudflareip(){
    if(isset($_SERVER['HTTP_CF_CONNECTING_IP']))
        return $_SERVER['HTTP_CF_CONNECTING_IP'];
    else
        return $_SERVER['REMOTE_ADDR'];
}
//Função Captcha google
function googlecaptcha($response){

    $vetParametros = array (
        "secret" => "6LfDETMUAAAAAEWf44BVdjq5mC5Nsykx71o2qb9d",
        "response" => $response,
        "remoteip" => cloudflareip()
    );

    //Abre a conexão e informa os parâmetros: URL, método POST, parâmetros e retorno numa string
    $curlReCaptcha = curl_init();
    curl_setopt($curlReCaptcha, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($curlReCaptcha, CURLOPT_POST, true);
    curl_setopt($curlReCaptcha, CURLOPT_POSTFIELDS, http_build_query($vetParametros));
    curl_setopt($curlReCaptcha, CURLOPT_RETURNTRANSFER, true);
    //A resposta é um objeto json em uma string, então só decodificar em um array (true no 2º parâmetro)
    $vetResposta = json_decode(curl_exec($curlReCaptcha), true);
    //Fecha a conexão
    curl_close($curlReCaptcha);
    return $vetResposta;
}