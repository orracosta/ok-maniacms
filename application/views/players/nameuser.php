<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('templates/header');
$this->load->view('templates/header-logo');
$this->load->view('templates/menu');
?>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body container">
                    <?php $this->load->view('templates/alert');?>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="home-perfil-bg min-h-150 imghd-card-t-l" style="background-image: url('<?=base_url('assets/images/habbo/15-article_image_newyear.gif')?>');">
                                <div class="row min-h-150 justify-content-end">
                                    <h4 class="text-border-white font-primary align-self-center m-r-30 m-l-5 text-uppercase"><i class="icofont icofont-user"></i><b class="m-l-10">Quase lá! Escolha seu nome de usuário!</b></h4>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block">
                                    <form action="<?=base_url("registro/regusuario")?>" method="post" accept-charset="utf-8">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label id="user-invalid"  class="m-l-5 col-form-label text-danger display-none"><b>*Nome de usuário inválido ou já registrado! Por favor, escolha outro nome de usuário.</b></label>
                                                <div id="reg-usuario" class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icofont icofont-user"></i>
                                                    </span>
                                                    <input id="regfield-username" name="username" type="text" class="form-control" placeholder="Escolha seu nome de usuário..." onchange="register_username(this.value)" required>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <button id="btn-registrar" class="btn btn-success btn-block m-t-5" disabled="disabled">Escolher Nome de Usuário!</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="home-perfil-bg min-h-150 imghd-card-t-l" style="background-image: url('<?=base_url('assets/images/habbo/spromo_tt.gif')?>');">
                                <div class="row min-h-150 justify-content-end">
                                    <h4 class="text-border-white font-danger align-self-center m-r-5 m-l-5 text-uppercase"><i class="icofont icofont-info"></i><b class="m-l-10">Dicas Importantes!</b></h4>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block">
                                    <p>
                                        <b>Dica:</b> Você deve utilizar somente traço(-), sublinhado(_), ponto(.), dois pontos(:), letras(a-z) e números(0-9).<br>Seu nome de usuário também deve conter de 3 à 15 caracteres.
                                        <div class="text-center">
                                            <img class="img-fluid m-b-10" src="<?=base_url("assets/images/habbo/this-habbo.png")?>" />
                                        </div>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php
$this->load->view('templates/footer');
?>