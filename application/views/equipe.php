<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('templates/header');
$this->load->view('templates/header-logo');
$this->load->view('templates/menu');
?>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body container">
                    <?php $this->load->view('templates/alert');?>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="home-perfil-bg min-h-180 imghd-card-t-l" style="background-image: url('<?=base_url('assets/images/habbo/turma-da-manutencao.gif')?>');">
                                <div class="row min-h-180 justify-content-end">
                                    <h2 class="text-border-white font-primary align-self-center m-r-30 m-l-5 text-uppercase"><i class="icofont icofont-team"></i><b class="m-l-10">Equipe Staff</b></h2>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block manialink">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <ul class="basic-list list-icons-img login-list-icons">
                                                <h6 class="text-uppercase">CEO's</h6>
                                                <hr>
                                                <?php
                                                foreach($rank_ceo as $rank => $array):
                                                    ?>
                                                    <li>
                                                        <img src="https://www.mania.gg/api/head/<?=$array['username']?>" class="img-fluid img-circle p-absolute d-block text-center habbo-head" alt="">
                                                        <h6> <?=$array['username']?>
                                                            <?php if($array['online'] == 1):?>
                                                                <label class="label label-success f-right">Online</label>
                                                            <?php else:?>
                                                                <label class="label label-danger f-right">Offline</label>
                                                            <?php endif; ?>
                                                        </h6>
                                                        <p><i class="icofont icofont-mail"></i> <span class="fie"><b>Missão: </b><?=$array['motto']?></span></p>
                                                        <p><i class="icofont icofont-time"></i> <span class="fie"><b>Última vez online: </b><?=date("d/m/Y - H:i", $array["last_online"])?></span></p>
                                                    </li>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </ul>
                                            <ul class="basic-list list-icons-img login-list-icons">
                                                <h6 class="text-uppercase">Administradores</h6>
                                                <hr>
                                                <?php
                                                foreach($rank_adm as $rank => $array):
                                                    ?>
                                                    <li>
                                                        <img src="https://www.mania.gg/api/head/<?=$array['username']?>" class="img-fluid img-circle p-absolute d-block text-center habbo-head" alt="">
                                                        <h6> <?=$array['username']?>
                                                            <?php if($array['online'] == 1):?>
                                                                <label class="label label-success f-right">Online</label>
                                                            <?php else:?>
                                                                <label class="label label-danger f-right">Offline</label>
                                                            <?php endif; ?>
                                                        </h6>
                                                        <p><i class="icofont icofont-mail"></i> <span class="fie"><b>Missão: </b><?=$array['motto']?></span></p>
                                                        <p><i class="icofont icofont-time"></i> <span class="fie"><b>Última vez online: </b><?=date("d/m/Y - H:i", $array["last_online"])?></span></p>
                                                    </li>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </ul>
                                            <ul class="basic-list list-icons-img login-list-icons">
                                                <h6 class="text-uppercase">Gerentes</h6>
                                                <hr>
                                                <?php
                                                foreach($rank_ger as $rank => $array):
                                                    ?>
                                                    <li>
                                                        <img src="https://www.mania.gg/api/head/<?=$array['username']?>" class="img-fluid img-circle p-absolute d-block text-center habbo-head" alt="">
                                                        <h6> <?=$array['username']?>
                                                            <?php if($array['online'] == 1):?>
                                                                <label class="label label-success f-right">Online</label>
                                                            <?php else:?>
                                                                <label class="label label-danger f-right">Offline</label>
                                                            <?php endif; ?>
                                                        </h6>
                                                        <p><i class="icofont icofont-mail"></i> <span class="fie"><b>Missão: </b><?=$array['motto']?></span></p>
                                                        <p><i class="icofont icofont-time"></i> <span class="fie"><b>Última vez online: </b><?=date("d/m/Y - H:i", $array["last_online"])?></span></p>
                                                    </li>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </ul>
                                            <ul class="basic-list list-icons-img login-list-icons">
                                                <h6 class="text-uppercase">Moderadores</h6>
                                                <hr>
                                                <?php
                                                foreach($rank_mod as $rank => $array):
                                                    ?>
                                                    <li>
                                                        <img src="https://www.mania.gg/api/head/<?=$array['username']?>" class="img-fluid img-circle p-absolute d-block text-center habbo-head" alt="">
                                                        <h6> <?=$array['username']?>
                                                            <?php if($array['online'] == 1):?>
                                                                <label class="label label-success f-right">Online</label>
                                                            <?php else:?>
                                                                <label class="label label-danger f-right">Offline</label>
                                                            <?php endif; ?>
                                                        </h6>
                                                        <p><i class="icofont icofont-mail"></i> <span class="fie"><b>Missão: </b><?=$array['motto']?></span></p>
                                                        <p><i class="icofont icofont-time"></i> <span class="fie"><b>Última vez online: </b><?=date("d/m/Y - H:i", $array["last_online"])?></span></p>
                                                    </li>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="home-perfil-bg min-h-180 imghd-card-t-l" style="background-image: url('<?=base_url('assets/images/habbo/maintenance_2.png')?>');">
                                <div class="row min-h-180 justify-content-end">
                                    <h2 class="text-border-white font-danger align-self-center m-r-5 m-l-5 text-uppercase"><i class="icofont icofont-code-alt"></i><b class="m-l-10">DEVS</b></h2>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block manialink">
                                    <ul class="basic-list list-icons-img login-list-icons">
                                        <?php
                                        foreach($rank_dev as $rank => $array):
                                            ?>
                                            <li>
                                                <img src="https://www.mania.gg/api/head/<?=$array['username']?>" class="img-fluid img-circle p-absolute d-block text-center habbo-head" alt="">
                                                <h6> <?=$array['username']?>
                                                    <?php if($array['online'] == 1):?>
                                                        <label class="label label-success f-right">Online</label>
                                                    <?php else:?>
                                                        <label class="label label-danger f-right">Offline</label>
                                                    <?php endif; ?>
                                                </h6>
                                                <p><i class="icofont icofont-mail"></i> <span class="fie"><b>Missão: </b><?=$array['motto']?></span></p>
                                                <p><i class="icofont icofont-time"></i> <span class="fie"><b>Última vez online: </b><?=date("d/m/Y - H:i", $array["last_online"])?></span></p>
                                            </li>
                                            <?php
                                        endforeach;
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php
$this->load->view('templates/footer');
?>