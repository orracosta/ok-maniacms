<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('templates/header');
$this->load->view('templates/header-logo');
$this->load->view('templates/menu');
?>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body container">
                    <?php $this->load->view('templates/alert');?>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php if(!$this->session->has_userdata('loggedIn')): ?>
                                        <div class="home-perfil-bg min-h-300 imghd-card-l" style="background-image: url('<?=base_url('assets/images/habbo/habbo-new.png')?>'); background-size: cover;">
                                            <div class="row justify-content-center min-h-300">
                                                <div class="col-lg-7 align-self-center p-l-20 p-r-20">
                                                    <a href="<?=base_url("registro")?>" class="btn btn-danger btn-block btn-login">Registre-se Agora!</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card card-perfilbg">
                                            <div class="card-block">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <ul class="basic-list list-icons-img login-list-icons">
                                                            <li>
                                                                <img src="<?=base_url("assets/images/habbo/appartement_text.png")?>" class="img-fluid p-absolute d-block text-center habbo-head m-t-30" alt="" />
                                                                <h6 class="text-uppercase">MAIS QUE UM JOGO!</h6>
                                                                <p>No Mania, você pode fazer inúmeras amizades, criar sua própria Família e se tornar o que quiser no hotel!</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <ul class="basic-list list-icons-img login-list-icons">
                                                            <li>
                                                                <img src="<?=base_url("assets/images/habbo/siege_text.png")?>" class="img-fluid p-absolute d-block text-center habbo-head m-t-30" alt="" />
                                                                <h6 class="text-uppercase">GRÁTIS E DIVERTIDO!</h6>
                                                                <p>Um jogo online gratuito onde você pode explorar quartos, fazer amigos e ganhar prêmios sem pagar!</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="home-perfil-bg min-h-300 imghd-card-b-l" style="background-image: url('<?=base_url('assets/images/habbo/e13_backdrop_l.png')?>')">
                                            <div class="row justify-content-center min-h-300">
                                                <img id="contorno-branco" src="https://www.mania.gg/api/bodylg/<?=$player['username']?>" class="perfil-bg-user">
                                                <div class="col-lg-6 align-self-center p-l-20 p-r-20">
                                                    <a href="<?=base_url("hotel")?>" class="btn btn-primary btn-block btn-login">ENTRAR NO HOTEL</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card card-perfilbg">
                                            <div class="card-block">
                                                <h6 class="text-uppercase">Bem-Vindo(a) <?=$player['username']?>!</h6>
                                                <hr>
                                                <ul class="basic-list">
                                                    <li>
                                                        <p><span class="fie"><b>Pontos de referência:</b> <?=$player['ref_points']?> Pontos</span></p>
                                                    </li>
                                                </ul>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icofont icofont-link"></i>
                                                    </span>
                                                    <input name="email" value="<?=base_url("r/").$player["username"]?>" type="text" class="form-control" placeholder="Seu Email atual" required disabled>
                                                </div>
                                                <hr>
                                                <p><b>Divulgue & Ganhe:</b> Agora você pode ajudar o Mania a crescer e ser recompensado. Utilizando o link acima para divulgar o hotel, cada novo usuário que se registrar utilizando seu link, você receberá 1 ponto de referência.<br>
                                                    Com os pontos de referência você poderá disputar o título de Top Divulgador no ranking e ainda trocar os pontos por raros e diamantes em nossa futura loja no site.
                                                </p>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>

                                <div class="col-lg-12">
                                    <div class="home-perfil-bg min-h-180 imghd-card-t-l" style="background-image: url('<?=base_url('assets/images/habbo/banzai-1.gif')?>');">
                                        <div class="row min-h-180 justify-content-end">
                                            <h3 class="text-border-white font-primary align-self-center m-r-30 m-l-5"><i class="icofont icofont-trophy"></i><b class="m-l-10"> RANKING DE EVENTOS</b></h3>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-block ">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <ul class="basic-list list-icons-img login-list-icons">
                                                        <h6 class="text-uppercase">TOP 3 da Semana</h6>
                                                        <hr>
                                                        <?php
                                                        $pos = 0;
                                                        foreach($hall_semanal as $hall => $hall_semanal):
                                                            ?>
                                                            <li>
                                                                <img src="https://www.mania.gg/api/head/<?=$hall_semanal['username']?>" class="img-fluid img-circle p-absolute d-block text-center habbo-head" alt="">
                                                                <h6> <?=$hall_semanal['username']?> <label class="label label-primary back-rank-<?=$pos?> f-right"><?=$pos+1?></label></h6>
                                                                <p><i class="icofont icofont-trophy color-rank-<?=$pos?>"></i> <span class="fie"><b>Pontos: </b><?=$hall_semanal['s_points']?> pontos</span></p>
                                                                <p><i class="icofont icofont-mail"></i> <span class="fie"><?=$hall_semanal['motto'] == "" ? "Mania Hotel <3" : $hall_semanal['motto']?></span></p>
                                                            </li>
                                                            <?php
                                                            $pos++;
                                                        endforeach;
                                                        ?>
                                                    </ul>
                                                </div>
                                                <div class="col-lg-6">
                                                    <ul class="basic-list list-icons-img login-list-icons">
                                                        <h6 class="text-uppercase">TOP 3 do Mês</h6>
                                                        <hr>
                                                        <?php
                                                        $pos = 0;
                                                        foreach($hall_mensal as $hall => $hall_mensal):
                                                            ?>
                                                            <li>
                                                                <img src="https://www.mania.gg/api/head/<?=$hall_mensal['username']?>" class="img-fluid img-circle p-absolute d-block text-center habbo-head" alt="">
                                                                <h6> <?=$hall_mensal['username']?> <label class="label label-primary back-rank-<?=$pos?> f-right"><?=$pos+1?></label></h6>
                                                                <p><i class="icofont icofont-trophy color-rank-<?=$pos?>"></i>  <span class="fie"><b>Pontos: </b><?=$hall_mensal['m_points']?> pontos</span></p>
                                                                <p><i class="icofont icofont-mail"></i>  <span class="fie"><?=$hall_mensal['motto'] == "" ? "Mania Hotel <3" : $hall_mensal['motto']?></span></p>
                                                            </li>
                                                            <?php
                                                            $pos++;
                                                        endforeach;
                                                        ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <?php $this->load->view('templates/login'); ?>
                            <div class="home-perfil-bg min-h-180 imghd-card-t" style="background-image: url('<?=base_url('assets/images/habbo/builderhabbo.png')?>');">
                                <div class="row min-h-180 justify-content-center">
                                    <h4 class="text-border-white font-danger align-self-center m-r-5 m-l-5"><i class="icofont icofont-trophy"></i><b class="m-l-10"> RANKING DE PROMOÇÕES</b></h4>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block">
                                    <ul class="basic-list list-icons-img login-list-icons m-t-10">
                                        <?php
                                        $pos = 0;
                                        foreach($hall_promo as $hall => $hall_promo):
                                            ?>
                                            <li>
                                                <img src="https://www.mania.gg/api/head/<?=$hall_promo['username']?>" class="img-fluid img-circle p-absolute d-block text-center habbo-head" alt="">
                                                <h6> <?=$hall_promo['username']?> <label class="label label-primary back-rank-<?=$pos?> f-right"><?=$pos+1?></label></h6>
                                                <p><i class="icofont icofont-trophy color-rank-<?=$pos?>"></i> <span class="fie"><b>Pontos: </b><?=$hall_promo['promo_pts']?> pontos</span></p>
                                                <p><i class="icofont icofont-mail"></i>   <span class="fie"><?=$hall_promo['motto'] == "" ? "Mania Hotel <3" : $hall_promo['motto']?></span></p>
                                            </li>
                                            <?php
                                            $pos++;
                                        endforeach;
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php
$this->load->view('templates/footer');
?>