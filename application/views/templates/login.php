<div class="card">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" style="border-top-left-radius: 5px;border-top-right-radius: 5px;" role="listbox">
            <?php
            $active = 1;
            foreach($cms_news as $news => $array):
            ?>
                <div class="carousel-item <?php if($active == 1){echo 'active';} ?>">
                    <img class="d-block img-fluid w-100" style="height:200px !important;" src="<?=$array["image"]?>" alt="First slide">
                    <div class="carousel-caption">
                        <a href="<?=base_url("noticia/").$array["id"].'/'.limpar($array['title'])?>"><h5><?=$array["title"]?></h5>
                        <p class="m-l-0 text-left"><?=$array["short_story"]?></p></a>
                    </div>
                </div>
                <?php
                $active = 0;
                endforeach;
                ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon m-l-5" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon m-l-5" aria-hidden="true"></span>
            <span class="sr-only">Próximo</span>
        </a>
    </div>
    <div class="card-block">
        <?php if(!$this->session->has_userdata('loggedIn')): ?>
            <form id="do-login">
                <div class="row">
                    <div class="col-lg-12">
                        <h6><i class="icofont icofont-user"></i><b class="m-l-10 text-uppercase"><?=$cms_status['active_players'], $cms_status['active_players'] == 1 ? ' USUÁRIO ONLINE' : ' USUÁRIOS ONLINE'?></b></h6>
                        <hr>
                    </div>
                    <div class="col-lg-12">
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="icofont icofont-user"></i>
                        </span>
                            <input id="login-name" name="username" type="text" class="form-control" placeholder="Digite seu usuário">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="icofont icofont-lock"></i>
                        </span>
                            <input id="login-password" name="password" type="password" class="form-control" placeholder="Digite sua senha">
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <button id="btn-login" class="btn btn-primary btn-block m-t-5"><i class="icofont icofont-login p-r-15"></i>Login</button>
                    </div>

                    <div class="col-lg-6">
                        <a id="btn-facebook" href="#" class="btn btn-facebook btn-block m-t-5" onclick="fb_login()"><i class="icofont icofont-social-facebook"></i>Facebook</a>
                    </div>

                    <div class="col-lg-12">
                        <a href="<?=base_url("registro");?>" class="btn btn-danger btn-block m-t-5">Não tem conta? Registre-se!</a>
                    </div>
                </div>
            </form>
        <?php else: ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-12">
                        <h6><i class="icofont icofont-user"></i><b class="m-l-10 text-uppercase"><?=$cms_status['active_players'], $cms_status['active_players'] == 1 ? ' USUÁRIO ONLINE' : ' USUÁRIOS ONLINE'?></b></h6>
                        <hr>
                    </div>
                    <div class="row">

                        <div class="col">
                            <ul class="basic-list">
                                <li>
                                    <p><span class="fie"><b>PONTOS & DIAMANTES:</b></span></p>
                                    <hr>
                                    <p><span class="fie"><b>Eventos:</b> <?=$player['g_points']?> Pontos</span></p>
                                    <p><span class="fie"><b>Promoções:</b> <?=$player['promo_pts']?> Pontos</span></p>
									<p><span class="fie"><b>Diamantes:</b> <?=$player['vip_points']?></span></p>
                                </li>
                            </ul>
                        </div>
                        <div class="col align-self-center">
                            <img src="<?=base_url("assets/images/habbo/game-habo.png")?>" />
                        </div>
                    </div>
                    <hr>
                    <a href="<?=base_url("hotel")?>" class="btn btn-danger btn-block btn-login m-t-5"">ENTRAR NO HOTEL</a>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>