<div class="row">
    <div class="col-lg-12">
        <div id="alert-error" class="alert alert-danger background-danger text-center display-none"></div>
        <div id="alert-success" class="alert alert-success background-success text-center display-none"></div>
    </div>
</div>