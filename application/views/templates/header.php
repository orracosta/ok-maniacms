<!DOCTYPE html>
<html lang="pt-br">

<head>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <title><?=$titulo?></title>

    <meta name="description" content="Mania Hotel: Crie seu Habbo, Construa seu quarto, Converse e Faça Novos Amigos! Novo Site, Novos Mobis, Novas Roupas e muito mais. Junte-se ao melhor Habbo Pirata do Brasil!"/>
    <meta name="keywords" content="habblet, habblethotel, ihabi, habbopop, habbopoop, hebbo, habblive, habb, lella, lellahotel,lella hotel, habbinfo, habbinfo hotel, habblive, habblive hotel, habbolatino, habbletlatino, habblet, habblethotel, crazzy, habb, habbhotel , furnis , mobs, client, cliente, client hotel, clienthotel, atualizado, catalogo, mania, maniahotel, mania hotel, "/>
    <meta name="apple-itunes-app" content="app-id=472937654"><meta name="google-play-app" content="app-id=com.cloudmosa.puffinFree">

    <meta property="og:image" content="<?=base_url("assets/images/img_facebook.png")?>">
    <link rel="shortcut icon" href="<?=base_url("assets/images/favicon.gif")?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url("assets/images/favicon.gif")?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url("assets/images/favicon.gif")?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url("assets/images/favicon.gif")?>">
    <link rel="apple-touch-icon" sizes="57x57" href="<?=base_url("assets/images/favicon.gif")?>">

    <link href="https://fonts.googleapis.com/css?family=Mada:300,400,500,600,700" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/bower_components/bootstrap/css/bootstrap.min.css?2020");?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/icon/themify-icons/themify-icons.css?2020");?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/pages/list-scroll/list.css?2020");?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/bower_components/stroll/css/stroll.css?2020");?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/icon/icofont/css/icofont.css?2020");?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/pages/flag-icon/flag-icon.min.css?2020");?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/pages/menu-search/css/component.css?2020");?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/css/style.css?2020");?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/css/sticky-footer.css?2020");?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/css/linearicons.css?2020");?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/css/simple-line-icons.css?2020");?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/css/ionicons.css?2020");?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/css/jquery.mCustomScrollbar.css?2020");?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url("assets/css/mania.css?2021");?>">
    <style>
        body{background-image: url('<?=base_url('assets/images/habbo/5507rr.png')?>');}
        .mania-header {background-image: url("<?=base_url('assets/images/mania/header-primavera.png')?>");}
    </style>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script>
		 (adsbygoogle = window.adsbygoogle || []).push({
			  google_ad_client: "ca-pub-7269783918840015",
			  enable_page_level_ads: true
		 });
	</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114714565-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-114714565-1');
	</script>
</head>

<body>

<div class="theme-loader">
    <div class="ball-scale">
        <div></div>
    </div>
</div>

<div class="pcoded-container">