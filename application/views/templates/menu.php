<nav class="navbar navbar-dark bg-dark navbar-expand-lg mania-menu">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto container">
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url()?>">Principal</a>
            </li>
            <?php if($this->session->has_userdata('loggedIn')): ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url("configuracoes")?>">Configurações</a>
                </li>
            <?php endif; ?>
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url("noticias")?>">Notícias</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url("equipe")?>">Equipe Staff</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url("hall-da-fama")?>">Hall da fama</a>
            </li>
            <?php if(!$this->session->has_userdata('loggedIn')): ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url("registro")?>">Registre-se</a>
                </li>
            <?php endif; ?>
            <?php if($this->session->has_userdata('loggedIn')): ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?=base_url("sair")?>">Sair</a>
                </li>
            <?php endif; ?>
            <!--
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Ranking
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="#">Ranking Geral</a>
                    <a class="dropdown-item" href="#">Ranking do Mês</a>
                    <a class="dropdown-item" href="#">Hall da Fama</a>
                </div>
            </li>
            -->
        </ul>
    </div>
</nav>