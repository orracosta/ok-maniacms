</div>

<div class="footer-space"></div>

<footer class="footer">
    <div class="container">
        <span class="text-muted">© Mania.GG 2009~<?=date("Y");?> - Desenvolvido por iPlezier</span>
    </div>
</footer>

<!--[if lt IE 9]>
<div class="ie-warning">
    <h1>Aviso!</h1>
    <p>Você está usando uma versão desatualizada do Internet Explorer, por favor acesse novamente <br/>com um dos navegadores recomendados a seguie.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="<?=base_url("assets/images/browser/chrome.png");?>" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="<?=base_url("assets/images/browser/firefox.png");?>" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="<?=base_url("assets/images/browser/opera.png");?>" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="<?=base_url("assets/images/browser/safari.png");?>" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="<?=base_url("assets/images/browser/ie.png");?>" alt="">
                    <div>IE (9 ou acima)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Desculpe pelo inconveniente!</p>
</div>
<![endif]-->

<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="<?=base_url("assets/bower_components/jquery/js/jquery.min.js?1010")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/bower_components/jquery-ui/js/jquery-ui.min.js?1010")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/bower_components/popper.js/js/popper.min.js?1010")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/bower_components/bootstrap/js/bootstrap.min.js?1010")?>"></script>

<script type="text/javascript" src="<?=base_url("assets/bower_components/jquery-slimscroll/js/jquery.slimscroll.js?1010")?>"></script>

<script type="text/javascript" src="<?=base_url("assets/bower_components/modernizr/js/modernizr.js?1010")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/bower_components/modernizr/js/css-scrollbars.js?1010")?>"></script>

<script type="text/javascript" src="<?=base_url("assets/bower_components/classie/js/classie.js?1010")?>"></script>

<script type="text/javascript" src="<?=base_url("assets/bower_components/i18next/js/i18next.min.js?1010")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js?1010")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js?1010")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/bower_components/jquery-i18next/js/jquery-i18next.min.js?1010")?>"></script>

<script type="text/javascript" src="<?=base_url("assets/js/script.js?1010")?>"></script>
<script src="<?=base_url("assets/js/pcoded.min.js?1010")?>"></script>
<script src="<?=base_url("assets/js/jquery.mCustomScrollbar.concat.min.js?1010")?>"></script>
<script src="<?=base_url("assets/js/jquery.mousewheel.min.js?1010")?>"></script>

<script src="<?=base_url("assets/js/scripts.js?1010")?>"></script>
<script src="<?=base_url("assets/js/rpx.js?1010")?>"></script>

</body>
</html>

