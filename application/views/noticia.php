<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('templates/header');
$this->load->view('templates/header-logo');
$this->load->view('templates/menu');
?>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body container">
                    <?php $this->load->view('templates/alert');?>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-block manialink">
                                    <div class="min-h-80" style="background-image: url('https://www.mania.gg/api/bodylg/<?=$last_news["author"]?>');background-position: -25px -50px; background-repeat: no-repeat;">
                                        <h3 class="font-primary text-uppercase p-t-30" style="padding-left: 100px;">
                                            <i class="icofont icofont-newspaper"></i><b class="m-l-10"><?=$last_news['title']?></b>
                                        </h3>
                                        <label class="label label-primary f-left"><?=$last_news["author"].' - '.date("d/m", $last_news["created_at"])?></label>
                                    </div>
                                    <hr>
                                    <?=$last_news['long_story']?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="home-perfil-bg min-h-110 imghd-card-t" style="background-image: url('<?=base_url('assets/images/habbo/guerejonews.png')?>');">
                                <div class="row min-h-110 justify-content-start">
                                    <h4 class="text-border-white font-danger align-self-center m-r-5 m-l-30 m-t-40"><i class="icofont icofont-news"></i><b class="m-l-10">Outras Noticias</b></h4>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block manialink">
                                    <ul class="basic-list">
                                        <?php foreach($cms_news as $news => $array):?>
                                            <li>
                                                <p><i class="icofont icofont-arrow-right text-primary"></i> <span class="fie"><a href="<?=base_url("noticia/").$array["id"].'/'.limpar($array['title'])?>"><?=$array['title']?></a></span>
                                                    <label class="label label-primary f-right"><?=date("d/m", $array["created_at"])?></label>
                                                </p>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php
$this->load->view('templates/footer');
?>