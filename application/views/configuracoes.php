<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('templates/header');
$this->load->view('templates/header-logo');
$this->load->view('templates/menu');
?>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body container">
                    <?php $this->load->view('templates/alert');?>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="home-perfil-bg min-h-210" style="background-image: url('<?=base_url('assets/images/habbo/oito-conf.png')?>');background-position: 30px top;">
                                <div class="row min-h-210 justify-content-end">
                                    <h2 class="text-border-white font-primary align-self-center m-r-30 m-l-5 text-uppercase"><i class="icofont icofont-ui-password"></i><b class="m-l-10">Alterar Senha</b></h2>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block">
                                    <form id="alterar-senha">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label id="senha-invalidantig"  class="m-l-5 col-form-label text-danger display-none"><b>*A senha digitada não coincide com a atual.</b></label>
                                                <div id="senha-antiga" class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icofont icofont-lock"></i>
                                                    </span>
                                                    <input id="regfield-senhaant" name="email" type="password" class="form-control" placeholder="Digite sua senha antiga..." onchange="check_passanti(this.value)" required>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <label id="senha-invalid"  class="m-l-5 col-form-label text-danger display-none"><b>*Sua nova senha deve conter no mínimo 6 caracteres!</b></label>
                                            </div>

                                            <div class="col-lg-6">
                                                <label id="senha-equal"  class="m-l-5 col-form-label text-danger display-none"><b>*Sua nova senha deve ser igual nos dois campos!</b></label>
                                            </div>

                                            <div class="col-lg-6">
                                                <div id="reg-senha" class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icofont icofont-lock"></i>
                                                    </span>
                                                    <input id="regfield-senha" name="senha" type="password" class="form-control" placeholder="Digite sua nova senha..." onchange="check_password_config(this.value);check_equals_config(('#regfield-senha-repeat').value)" required>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div id="reg-senha-repeat" class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icofont icofont-lock"></i>
                                                    </span>
                                                    <input id="regfield-senha-repeat" name="senhaTwo" type="password" class="form-control" placeholder="Digite sua nova senha novamente..." onchange="check_equals_config(this.value)" required>
                                                </div>
                                            </div>

                                            <div class="col-lg-12 bottom m-t-15">
                                                <button id="btn-registrar" class="btn btn-success btn-block m-t-5" disabled="disabled">Alterar Senha!</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="home-perfil-bg min-h-180 imghd-card-t-r" style="background-image: url('<?=base_url('assets/images/habbo/up-94e4518d67.gif')?>');">
                                <div class="row min-h-180 justify-content-start">
                                    <h3 class="text-border-white font-primary align-self-center m-r-30 m-l-5 text-uppercase"><i class="icofont icofont-email"></i><b class="m-l-10">Email Registrado em sua conta!</b></h3>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icofont icofont-email"></i>
                                                    </span>
                                                    <input name="email" value="<?=$player["email"]?>" type="text" class="form-control" placeholder="Seu Email atual" required disabled>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="home-perfil-bg min-h-150 imghd-card-t-l" style="background-image: url('<?=base_url('assets/images/habbo/spromo_tt.gif')?>');">
                                <div class="row min-h-150 justify-content-end">
                                    <h4 class="text-border-white font-danger align-self-center m-r-5 m-l-5 text-uppercase"><i class="icofont icofont-info"></i><b class="m-l-10">Dicas Importantes!</b></h4>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block">
                                    <p>
                                        <b>Dica 1:</b> Sua senha deve ter no mínimo 6 caracteres.<br>Uma grande dica, é você utilizar espaços, caracteres especiais(como # ou @) e números.
                                        <br><br>
                                        <b>Dica 2:</b> Caso utilize o Facebook para fazer login no hotel, a função alterar senha não irá funcionar em sua conta.
                                    <div class="text-center">
                                        <img class="img-fluid m-b-10" src="<?=base_url("assets/images/habbo/habbo-pass.png")?>" />
                                    </div>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php
$this->load->view('templates/footer');
?>