﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <title><?=$titulo?></title>

    <meta name="description" content="Mania Hotel: Crie seu Habbo, Construa seu quarto, Converse e Faça Novos Amigos! Novo Site, Novos Mobis, Novas Roupas e muito mais. Junte-se ao melhor Habbo Pirata do Brasil!"/>
    <meta name="keywords" content="habblet, habblethotel, ihabi, habbopop, habbopoop, hebbo, habblive, habb, lella, lellahotel,lella hotel, habbinfo, habbinfo hotel, habblive, habblive hotel, habbolatino, habbletlatino, habblet, habblethotel, crazzy, habb, habbhotel , furnis , mobs, client, cliente, client hotel, clienthotel, atualizado, catalogo, mania, maniahotel, mania hotel, "/>
    <meta name="apple-itunes-app" content="app-id=472937654"><meta name="google-play-app" content="app-id=com.cloudmosa.puffinFree">

    <meta property="og:image" content="<?=base_url("assets/images/img_facebook.png")?>">
    <link rel="shortcut icon" href="<?=base_url("assets/images/favicon.gif")?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url("assets/images/favicon.gif")?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url("assets/images/favicon.gif")?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url("assets/images/favicon.gif")?>">
    <link rel="apple-touch-icon" sizes="57x57" href="<?=base_url("assets/images/favicon.gif")?>">

    <meta property="og:title" content="<?=$titulo?>" />
    <meta property="og:description" content="Mania Hotel: Crie seu Habbo, Construa seu quarto, Converse e Faça Novos Amigos! Novo Site, Novos Mobis, Novas Roupas e muito mais. Junte-se ao melhor Habbo Pirata do Brasil!" />
    <meta property="og:url" content="https://www.mania.gg/" />

    <link rel="stylesheet" href="<?=base_url("/assets/css/client.css")?>" type="text/css">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

    <script type="text/javascript" src="<?=base_url("assets/bower_components/jquery/js/jquery.min.js")?>"></script>
    <script type="text/javascript" src="<?=base_url("assets/bower_components/jquery-ui/js/jquery-ui.min.js")?>"></script>
    <script type="text/javascript" src="<?=base_url("assets/js/swfobject.js")?>"></script>
    <script type="text/javascript" src="<?=base_url("assets/js/habboapi.js")?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(window).focus(function(){
                $("#k").show();
            }).blur(function(){
                $("#k").hide();
            })
        });
    </script>


    <style>
        #client, #light{
            font-family: "Segoe UI", sans-serif;
            font-size:14px;
            color: white;
        }
        hr{
            border: 0;
            border-top: 1px dashed rgba(255, 255, 255, 0.3);
            margin-top: 1rem;
            margin-bottom: 1rem;
        }
        #client a{
            color: #36b7e8 !important;
            text-decoration: underline !important;
        }
        #client a:hover{
            color: #238bab !important;
            text-decoration: underline !important;
        }
        #client a:visited{
            color: #36b7e8 !important;
            text-decoration: underline !important;
        }
        .menu_hotel {
            cursor: pointer;
            background-color: #454545;
            width: auto;
            height: 40px;
            padding: 20px;
            padding-left: 10px;
            padding-right: 10px;
            border: 2px solid #767676 !important;
            margin: 0px;
            font-size: 13px;
            float: left;
            font-weight: 500;
            color: white !important;
            display: block;
            white-space: nowrap;
            position: relative;
            margin-top: -65px;
            margin-left: 5px;
            border-radius: 5px;
            -moz-opacity: 0.6;
            opacity: 0.6;
            filter: alpha(opacity=60);
            font-family: "Segoe UI", sans-serif;
        }
        .menu_hotel:hover {
            -moz-opacity: 0.8;
            opacity: 0.8;
            filter: alpha(opacity=80);
        }
        .black_overlay{
            display: none;
            position: absolute;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 150%;
            background-color: black;
            z-index:1001;
            -moz-opacity: 0.8;
            opacity:.80;
            filter: alpha(opacity=80);
        }

        .white_content {
            display: none;
            position: absolute;
            top: 5%;
            left: 25%;
            width: 50%;
            height: 80%;
            padding: 16px;
            border: 16px solid orange;
            background-color: #121212  ;
            z-index:1002;
            overflow: auto;
        }
        .white_content p {
            color: #FFFF;
            font-family: "Segoe UI", sans-serif;
        }
    </style>

    <script type="text/javascript">
        function toggleFullScreen() {
            if ((document.fullScreenElement && document.fullScreenElement !== null) ||
                (!document.mozFullScreen && !document.webkitIsFullScreen)) {
                if (document.documentElement.requestFullScreen) {
                    document.documentElement.requestFullScreen();
                } else if (document.documentElement.mozRequestFullScreen) {
                    document.documentElement.mozRequestFullScreen();
                } else if (document.documentElement.webkitRequestFullScreen) {
                    document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                }
            } else {
                if (document.cancelFullScreen) {
                    document.cancelFullScreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitCancelFullScreen) {
                    document.webkitCancelFullScreen();
                }
            }
        }
        function resizeClient(){
            var theClient = document.getElementById('client');
            var theWidth = theClient.clientWidth;
            theClient.style.width = "10px";
            theClient.style.width = theWidth + "px";
        }
    </script>

    <style type="text/css">a:link,a:visited{text-decoration:none;color:#FFF;}</style>
    <script>
        function auto(){
            document.getElementById('player').volume = 0.4
        }
    </script>

    <script type="text/javascript">
        var flashvars = {
            "client.allow.cross.domain" : "1",
            "client.notify.cross.domain" : "0",
            "connection.info.host" : "<?php echo $client['client_ip']; ?>",
            "connection.info.port" : "<?php echo $client['client_port']; ?>",
            "site.url" : "<?php echo $client['client_url_prefix']; ?>",
            "url.prefix" : "<?php echo $client['client_url_prefix']; ?>",
            "client.reload.url" : "<?php echo $client['client_url_prefix']; ?>/hotel",
            "client.fatal.error.url" : "<?php echo $client['client_url_prefix']; ?>/hotel",
            "logout.url" : "<?php echo $client['client_url_prefix']; ?>/logout",
            "logout.disconnect.url" : "<?php echo $client['client_url_prefix']; ?>/client",
            "client.connection.failed.url" : "<?php echo $client['client_url_prefix']; ?>/client",
            "external.variables.txt" : "<?php echo $client['variables']; ?>",
            <?php if (!empty($client['override_variables'])): ?>
            "external.override.variables.txt" : "<?php echo $client['override_variables']; ?>",
            <?php endif; ?>
            "external.texts.txt" : "<?php echo $client['flash_texts']; ?>",
            <?php if (!empty($client['override_flash_texts'])): ?>
            "external.override.texts.txt" : "<?php echo $client['override_flash_texts']; ?>",
            <?php endif; ?>
            "productdata.load.url" : "<?php echo $client['product_data']; ?>",
            "furnidata.load.url" : "<?php echo $client['furni_data']; ?>",
            "photo.upload.url" : "<?php echo str_replace('{0}', $player, $client['photo_upload']); ?>",
            "avatareditor.promohabbos": "https://www.habbo.com.br/api/public/lists/hotlooks",
            "use.sso.ticket" : "1",
            "sso.ticket" : "<?php echo $ticket; ?>",
            "processlog.enabled" : "0",
            "client.starting.revolving" : "Quando você menos esperar...terminaremos de carregar.../Carregando mensagem divertida! Por favor espere./Você quer batatas fritas para acompanhar?/Siga o pato amarelo./O tempo é apenas uma ilusão./Já chegamos?!/Eu gosto da sua camiseta./Olhe para um lado. Olhe para o outro. Pisque duas vezes. Pronto!/Não é você, sou eu./Shhh! Estou tentando pensar aqui./Carregando o universo de pixels.",
            "flash.client.url" : "<?php echo $client['flash_client']; ?>",
            "flash.client.origin" : "popup",
        };
    </script>

    <script type="text/javascript">
        var params = {
            "base" : "<?php echo $client['flash_client']; ?>",
            "allowScriptAccess" : "always",
            "menu" : "false",
            "wmode": "opaque"
        };

        swfobject.embedSWF('<?php echo $client['flash_client']; echo $client['client_swf'] ?>', 'client', '100%', '100%', '11.1.0', '//images.habbo.com/habboweb/63_1d5d8853040f30be0cc82355679bba7c/10449/web-gallery/flash/expressInstall.swf', flashvars, params, null, null);
    </script>
</head>

<body>
<img src="<?=base_url('assets/images/client/banner.gif')?>?" width="1" height="1" id="XcVCCW">
<div id="light" class="white_content" style="text-align: center">
    <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'" style="text-decoration:none;position:absolute;top:0px;left:0px;display:none;"></a>
    <p><img style="margin: 30px" src="<?=base_url('assets/images/client/YFsJsqy.png')?>"><br></p>
    <p>
    <h1>Ooops! Desative o bloqueador de anúncios!</h1><br>
    </p>
    <p>
        Foi detectado um(a) plugin/extensão de bloqueio de anúncios em seu navegador.
        Para manter nosso site, precisamos exibir anúncios.<br>
        Seu acesso continuará negado enquanto esse(a) plugin/extensão estiver ativado(a).
        <br>
        <br>
    </p>
    <p>
    <h3>Porque evitamos Adblock?</h3>
    <br>Este site é mantido pela monetização de anúncios, então necessitamos que todos anúncios sejam exibidos corretamente.
    <br>
    <br>
    </p>
    <p>
    <h3>Como pausar o AdBlock:</h3>
    <br>No canto superior direito do Chrome ou Firefox, clique no ícone da ABP, e desmarque a caixa "Ativar Adblock Plus".
    <br>
    <br>
    </p>
    <p><br><a href="/client"><img src="<?=base_url('assets/images/client/ufAcyI0.png')?>"></a><br><br></p>
    <p>
    <hr>
    <h2>Chrome ou Firefox</h2>
    <hr>
    <h3>#1 (Pausar AdBlock):</h3><br/>
    <img src="<?=base_url('assets/images/client/yl2PJuF.png')?>">
    <hr>
    <h3>#2 (Pausar AdBlock):</h3><br/>
    <img src="<?=base_url('assets/images/client/DrUTvYT.png')?>">
    </p>
</div>
<div id="fade" class="black_overlay"></div>
<script type="text/javascript">
    function cicklow_XcVCCW(){
        if(document.getElementById("XcVCCW")==null || document.getElementById("XcVCCW").offsetHeight<=0){
            document.getElementById('light').style.display='block';
            document.getElementById('fade').style.display='block'
        }
    }
    window.onload=cicklow_XcVCCW;
</script>
<div id="client">
    <div id="flash-wrapper">
        <div id="flash-container">
            <div id="content" style="width: 700px;padding-top: 180px; margin: 20px auto 0 auto; text-align: center">
                <p><img src="<?=base_url('assets/images/client/YFsJsqy.png')?>" style="margin-bottom: 50px;"></p>
                <p><h1 style="font-family: sans-serif;">Você ativar o Flash para jogar o Mania!</h1><br></p>
                <p>Se você está usando o computador para jogar, você precisa <a href="http://www.adobe.com/go/getflashplayer" target="_blank">permitir ou atualizar o Flash</a>.<br>
                    Para jogar o Mania, por favor <b>clique no botão abaixo</b>. Após clicar no botão, clique em permitir!<br><br>
                    <a href="http://www.adobe.com/go/getflashplayer" target="_blank"><img src="<?=base_url('assets/images/client/cNNnBlW.png')?>"></a>
                </p>
                <p><br>
                    OBS: se você bloqueou o Flash, você terá que ir nas configurações do seu navegador para desbloquear o plugin e poder o hotel. Caso contrário, não será possível seguir jogando.
                </p>
                <p>
                    <br><br>
                    Se você tem um iPhone ou Android você pode baixar o seguinte aplicativo para jogar o Mania Hotel!<br />
                    <br>
                    <a target="_blank" title="Apple" href="https://itunes.apple.com/fr/app/puffin-web-browser/id472937654?mt=8"><img src="<?=base_url('assets/images/client/kZpVlX9.png')?>"></a>&nbsp;&nbsp;
                    <a target="_blank" title="Android" href="https://play.google.com/store/apps/details?id=com.cloudmosa.puffinFree&hl=fr"><img src="<?=base_url('assets/images/client/hgpOvO5.png')?>"></a>
                </p>
            </div>
            <div id="content" class="client-content"></div>
        </div>
    </div>
</div>
<div id="client-ui">
    <div id="k">
    </div>
    <!--guerejo-->
    <div>
        <script src="<?=base_url("assets/images/client/jquery.timer.js")?>" type="text/javascript"></script>
        <script type="text/javascript">
            // Ads redone by Boss - big bad boss man
            $(document).ready(function() {
                showAd();
                adTimer.play();
            });

            var Interval = 7200000; // 20 minutes;
            var I = 30;
            var timer = $.timer(function() {
                I = 30;
                showAd();
                adTimer.play();
            });
            timer.set({
                time: Interval,
                autostart: true
            });

            var adClosing = $.timer(function() {
                closeAd();
                adClosing.stop();
            });

            adClosing.set({
                time: 30000,
                autostart: false
            });

            var adTimer = $.timer(function() {
                I--;
                $("#adTimer").html("Anúncio fecha e <b>" + I + "</b> segundos!");
                if (I <= 0)
                    adTimer.stop();
            });

            adTimer.set({
                time: 1000,
                autostart: false
            });

            function showAd() {
                $("#room").fadeIn("slow");
                adClosing.play();
                timer.stop();
            }

            function closeAd() {
                $("#room").fadeOut("slow");
                timer.play();
            }
        </script>
        <div id="room" class="roomenterad-Leet-container" style="display:inline-block;left: 50%; margin-left: -220px; margin-top: -50px; position: fixed; height: 165px; width: 490px; background-image: url('<?=base_url('assets/images/client/banner_live-02.png')?>'); background-repeat: no-repeat no-repeat; z-index: 99999;">
            <div id="adTimer" style="position: absolute; left: 6px; top: 76px; color: rgb(255, 255, 255); font-size: 10px; font-family: Verdana, sans-serif; visibility: visible;float:left;" class="roomenterad-closing1">O Anúncio fechará em <b>30</b> segundos!</div>
            <div id="closeAd" style="position: absolute; right: 13px; top: 76px; color: rgb(255, 255, 255); font-size: 10px; font-family: Verdana, sans-serif; visibility: visible;float:right;" class="roomenterad-closing1"><a href="#" style="color:white;font-weight:bold" onclick="closeAd();">Fechar Anuncio</a></div>
            <div style="position: absolute; left: 6px; top: 91px" class="roomenterad-Leet-thead">
                <!-- ads pequeno-->
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- cliente pequeno -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:468px;height:60px"
                     data-ad-client="ca-pub-7269783918840015"
                     data-ad-slot="6779179275"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
                <!-- fim ads pequeno-->
                <div id="room" class="roomenterad-habblet-container" style="bottom: 0px; display: inline-block; left: 50%; margin-left: -370px; margin-bottom: -30px; position: fixed; height: 195px; width: 740px;
                        background-image: url('<?=base_url('assets/images/client/pAuyqOa.png')?>'); background-repeat: no-repeat no-repeat; z-index: 99999">
                    <div id="adTimer" style="position: absolute; right: 9px; bottom: 80px;color: #ffffff; font-weight: bold; font-size: 10px; font-family: Verdana, sans-serif; cursor: pointer;" class="roomenterad-closing1">O Anúncio fechará em <b>30</b> segundos!</div>
                    <div id="closeAd" style="position: absolute; right: 6px; top: 76px; color: rgb(255, 255, 255); font-size: 10px; font-family: Verdana, sans-serif; visibility: visible;float:right;" class="roomenterad-closing1"><a href="#" style="color:white;font-weight:bold" onclick="closeAd();">Fechar Anuncio</a></div>
                    <div style="position: absolute; left: 6px; bottom: 95px" class="roomenterad-habblet-thead">
                        <!-- ads normal-->
                        <!-- Cliente 728 -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:728px;height:90px"
                             data-ad-client="ca-pub-7269783918840015"
                             data-ad-slot="6271364071"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                        <!-- fim ads normal-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--fim  guerejo-->
    <div class="menu_hotel" onclick="toggleFullScreen()">
        <div style="margin-top:36px;"><font style="font-size:15px"><i class="fas fa-expand-arrows-alt"></i></font></div>
    </div>
    <div class="menu_hotel" onclick="resizeClient()">
        <div style="margin-top:36px;"><font style="font-size:15px"><i class="far fa-snowflake"></i></font>&nbsp;&nbsp;DESCONGELAR</div>
    </div>

    <div id="content" class="client-content"></div>

    <!-- PLAYER -->
    <div id="area_player" style="top: 0;position: fixed;z-index: 2;">
        <!-- PRINCIPAL -->
        <div id="player" class="draggable ui-widget-content ui-draggable" style="left: 380.313px; position: relative; top: 0px;">
            <!-- PLAYER MIN -->
            <div class="player_min">
                <div class="guy"></div>
                <div class="txt"><a href="http://maniahitz.com" target="_blank"><span class="marked">ManiaHitz</span></a></div>
                <div class="open o-c tip" title=""></div>
            </div>
            <!-- FIM PLAYER MIN -->
            <script type="text/javascript">
                function atualiza_dados(id, caminho)
                {
                    $("#" + id).html("...");
                    $("#" + id).fadeOut(1000);
                    $("#" + id).load("<?=base_url("player/status.php?ver=")?>" + caminho);
                    $("#" + id).fadeIn(2000);
                }

            </script>
            <audio src="http://167.114.53.24:9986/;" id="audio" autoplay class="maniahitz" volume="30"></audio>
            <script type="text/javascript">
                var stream = document.getElementById("audio");
                $("#audio").prop("volume", 0.3);
                function UpdateAudio(){
                    var update = document.getElementById('audio'); audio.src='http://167.114.53.24:9986/;'; audio.load();
                    atualiza_dados('djver', 'dj'), atualiza_dados('programaver', 'programa'), atualiza_dados('ouvintesver', 'ouvintes'), atualiza_dados('imgver', 'imgloc');
                }
            </script>

            <!-- PLAYER  -->
            <div class="player">
                <div class="imageavater">
                    <div id="imgloc"><a onclick="atualiza_dados('imgver','imgloc')"><span id="imgver" style="cursor:pointer"></span></a></div>
                    <img src="https://www.mania.gg/api/body/VitrolaMania" alt="">
                </div>
                <div class="separa"></div>

                <div style="margin-top:15px">
                    <div id="locutor" class="info"><i class="fas fa-user"></i>&nbsp;&nbsp; <a onclick="atualiza_dados('djver','dj')"><span id="djver" style="cursor:pointer"></span></a></div>
                    <div id="programa" class="info"><i class="fas fa-headphones"></i>&nbsp;&nbsp; <a onclick="atualiza_dados('programaver','programa')"><span id="programaver"" style="cursor:pointer"></span></a></div>
                    <div id="ouvintes" class="info"><i class="fas fa-users"></i>&nbsp; <a onclick="atualiza_dados('ouvintesver','ouvintes')"><span id="ouvintesver" style="cursor:pointer"></span></a></div>
                </div>

                <div class="close o-c tip" title=""></div>
                <!--<a href="http://maniahitz.com/pedidos"
                   onclick="window.open(this.href,'targetWindow',
                                   'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=no, width=640, height=260');
                                   return false;">
                    <div class="site tip" title="Fazer um pedido" style="cursor:pointer"></div>
                </a>-->
                <div class="UpdateAudio" onclick="UpdateAudio()"><i class="fas fa-sync"></i></div>
                <div class="playerpe" title="Play/Pause" onClick="mutar()"><i class="faplay fas fa-pause"></i></div>
                <div class="handle ui-draggable-handle"><i class="fas fa-arrows-alt-h"></i></div>
                <div id="volume">
                    <p id="volumeHidden" hidden>30</p>
                    <div id="barra"></div>
                </div>
            </div>
            <!-- FIM PLAYER -->
        </div>
    </div>
    <!-- FIM PLAYER -->
</div>

<link rel="stylesheet" type="text/css" href="<?=base_url("player/includes/radio/reset.css?56")?>" />
<link rel="stylesheet" type="text/css" href="<?=base_url("player/includes/radio/style.css?53")?>"/>
<link rel="stylesheet" type="text/css" href="<?=base_url("player/includes/radio/tipped.css?53")?>"/>
<link rel="stylesheet" type="text/css" href="<?=base_url("player/includes/radio/maniahitz.css?53")?>">
<script src="<?=base_url("player/includes/js/jquery.js?53")?>"></script>
<script src="<?=base_url("player/includes/js/jquery-ui.js?53")?>"></script>
<script src="<?=base_url("player/includes/js/tipped.js?53")?>"></script>
<script src="<?=base_url("player/includes/js/maniahitz.js?53")?>"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $(document).on("click", "#playerButton", function ()
        {
            if ($("#playerButton").attr('data-enable') == 0)
            {
                document.getElementById('audio').muted = false;
                $("#playerButton").attr('data-enable', '1');
            }
            else
            {
                document.getElementById('audio').muted = true;
                $("#playerButton").attr('data-enable', '0');
            }
        });
        atualiza_dados('djver','dj'),atualiza_dados('programaver','programa'),atualiza_dados('ouvintesver','ouvintes'),atualiza_dados('imgver','imgloc');
    });
</script>
</body>
</html>
