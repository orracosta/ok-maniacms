<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('templates/header');
$this->load->view('templates/header-logo');
$this->load->view('templates/menu');
?>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body container">
                    <?php $this->load->view('templates/alert');?>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="home-perfil-bg min-h-180 imghd-card-t-l" style="background-image: url('<?=base_url('assets/images/habbo/banzai-1.gif')?>');">
                                <div class="row min-h-180 justify-content-end">
                                    <h3 class="text-border-white font-primary align-self-center m-r-30 m-l-5"><i class="icofont icofont-trophy"></i><b class="m-l-10"> HALL DE EVENTOS</b></h3>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block manialink">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <ul class="basic-list list-icons-img login-list-icons">
                                                <h6 class="text-uppercase">Semanal</h6>
                                                <hr>
                                                <?php
                                                $pos = 0;
                                                foreach($hall_semanal as $hall => $hall_semanal):
                                                    ?>
                                                    <li>
                                                        <img src="https://www.mania.gg/api/head/<?=$hall_semanal['username']?>" class="img-fluid img-circle p-absolute d-block text-center habbo-head" alt="">
                                                        <h6> <?=$hall_semanal['username']?> <label class="label label-primary back-rank-<?=$pos?> f-right"><?=$pos+1?></label></h6>
                                                        <p><?php if($pos < 3):?><i class="icofont icofont-trophy color-rank-<?=$pos?>"></i><?php endif;?> <span class="fie"><b>Pontos: </b><?=$hall_semanal['s_points']?> pontos</span></p>
                                                    </li>
                                                    <?php
                                                    $pos++;
                                                endforeach;
                                                ?>
                                            </ul>
                                        </div>
                                        <div class="col-lg-4">
                                            <ul class="basic-list list-icons-img login-list-icons">
                                                <h6 class="text-uppercase">Mensal</h6>
                                                <hr>
                                                <?php
                                                $pos = 0;
                                                foreach($hall_mensal as $hall => $hall_mensal):
                                                    ?>
                                                    <li>
                                                        <img src="https://www.mania.gg/api/head/<?=$hall_mensal['username']?>" class="img-fluid img-circle p-absolute d-block text-center habbo-head" alt="">
                                                        <h6> <?=$hall_mensal['username']?> <label class="label label-primary back-rank-<?=$pos?> f-right"><?=$pos+1?></label></h6>
                                                        <p><?php if($pos < 3):?><i class="icofont icofont-trophy color-rank-<?=$pos?>"></i><?php endif;?>  <span class="fie"><b>Pontos: </b><?=$hall_mensal['m_points']?> pontos</span></p>
                                                    </li>
                                                    <?php
                                                    $pos++;
                                                endforeach;
                                                ?>
                                            </ul>
                                        </div>
                                        <div class="col-lg-4">
                                            <ul class="basic-list list-icons-img login-list-icons">
                                                <h6 class="text-uppercase">Global</h6>
                                                <hr>
                                                <?php
                                                $pos = 0;
                                                foreach($hall_global as $hall => $hall_global):
                                                    ?>
                                                    <li>
                                                        <img src="https://www.mania.gg/api/head/<?=$hall_global['username']?>" class="img-fluid img-circle p-absolute d-block text-center habbo-head" alt="">
                                                        <h6> <?=$hall_global['username']?> <label class="label label-primary back-rank-<?=$pos?> f-right"><?=$pos+1?></label></h6>
                                                        <p><?php if($pos < 3):?><i class="icofont icofont-trophy color-rank-<?=$pos?>"></i><?php endif;?>  <span class="fie"><b>Pontos: </b><?=$hall_global['g_points']?> pontos</span></p>
                                                    </li>
                                                    <?php
                                                    $pos++;
                                                endforeach;
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="home-perfil-bg min-h-180 imghd-card-t" style="background-image: url('<?=base_url('assets/images/habbo/builderhabbo.png')?>');">
                                <div class="row min-h-180 justify-content-center">
                                    <h4 class="text-border-white font-danger align-self-center m-r-5 m-l-5"><i class="icofont icofont-trophy"></i><b class="m-l-10"> HALL DE PROMOÇÕES</b></h4>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block manialink">
                                    <ul class="basic-list list-icons-img login-list-icons">
                                        <h6 class="text-uppercase">-</h6>
                                        <hr>
                                        <?php
                                        $pos = 0;
                                        foreach($hall_promo as $hall => $hall_promo):
                                            ?>
                                            <li>
                                                <img src="https://www.mania.gg/api/head/<?=$hall_promo['username']?>" class="img-fluid img-circle p-absolute d-block text-center habbo-head" alt="">
                                                <h6> <?=$hall_promo['username']?> <label class="label label-danger back-rank-<?=$pos?> f-right"><?=$pos+1?></label></h6>
                                                <p><?php if($pos < 3):?><i class="icofont icofont-trophy color-rank-<?=$pos?>"></i><?php endif;?>  <span class="fie"><b>Pontos: </b><?=$hall_promo['promo_pts']?> pontos</span></p>
                                            </li>
                                            <?php
                                            $pos++;
                                        endforeach;
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php
$this->load->view('templates/footer');
?>