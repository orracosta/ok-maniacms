<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view('templates/header');
$this->load->view('templates/header-logo');
$this->load->view('templates/menu');
?>

    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body container">
                    <?php $this->load->view('templates/alert');?>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="home-perfil-bg min-h-150 imghd-card-t-l" style="background-image: url('<?=base_url('assets/images/habbo/15-article_image_newyear.gif')?>');">
                                <div class="row min-h-150 justify-content-end">
                                    <h2 class="text-border-white font-primary align-self-center m-r-30 m-l-5"><i class="icofont icofont-user"></i><b class="m-l-10">CRIE SUA CONTA ABAIXO!</b></h2>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block">
                                    <form action="<?=base_url("registro/registrar")?>" method="post" accept-charset="utf-8">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label id="user-invalid"  class="m-l-5 col-form-label text-danger display-none"><b>*Nome de usuário inválido ou já registrado! Por favor, escolha outro nome de usuário.</b></label>
                                                <div id="reg-usuario" class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icofont icofont-user"></i>
                                                    </span>
                                                    <input id="regfield-username" name="username" type="text" class="form-control" placeholder="Escolha seu nome de usuário..." onchange="check_username(this.value)" required>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <label id="email-invalid"  class="m-l-5 col-form-label text-danger display-none"><b>*Email inválido ou já registrado! Por favor, insira um email válido.</b></label>
                                                <div id="reg-email" class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icofont icofont-email"></i>
                                                    </span>
                                                    <input id="regfield-email" name="email" type="text" class="form-control" placeholder="Digite seu email..." onchange="check_email(this.value)" required>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <label id="senha-invalid"  class="m-l-5 col-form-label text-danger display-none"><b>*Sua senha deve conter no mínimo 6 caracteres!</b></label>
                                            </div>

                                            <div class="col-lg-6">
                                                <label id="senha-equal"  class="m-l-5 col-form-label text-danger display-none"><b>*Sua senha deve ser igual nos dois campos!</b></label>
                                            </div>

                                            <div class="col-lg-6">
                                                <div id="reg-senha" class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icofont icofont-lock"></i>
                                                    </span>
                                                    <input id="regfield-senha" name="senha" type="password" class="form-control" placeholder="Digite sua senha..." onchange="check_password(this.value);check_equals(('#regfield-senha-repeat').value)" required>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div id="reg-senha-repeat" class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icofont icofont-lock"></i>
                                                    </span>
                                                    <input id="regfield-senha-repeat" name="senhaTwo" type="password" class="form-control" placeholder="Digite sua senha novamente..." onchange="check_equals(this.value)" required>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="g-recaptcha" data-sitekey="6LfDETMUAAAAAMLRPOGPNqxEZKgYSwgGr1w8gIjE" data-callback="check_regerrors"></div>
                                            </div>

                                            <div class="col-lg-6">
                                                <label  class="m-t-20 col-form-label text-info"><b>*Confirme o reCAPTCHA para poder se registrar.</b></label>
                                            </div>

                                            <div class="col-lg-12 bottom m-t-15">
                                                <button id="btn-registrar" class="btn btn-success btn-block m-t-5" disabled="disabled">Registre-se Agora!</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="home-perfil-bg min-h-150 imghd-card-t-l" style="background-image: url('<?=base_url('assets/images/habbo/spromo_tt.gif')?>');">
                                <div class="row min-h-150 justify-content-end">
                                    <h4 class="text-border-white font-danger align-self-center m-r-5 m-l-5 text-uppercase"><i class="icofont icofont-info"></i><b class="m-l-10">Dicas Importantes!</b></h4>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-block">

                                    <p>
                                        <b>Dica 1:</b> Insira um email válido, pois em nosso hotel o mesmo é de grande importancia para recuperação de senhas e suporte. O mesmo não poderá ser alterado depois!<br><br>
                                        <b>Dica 2:</b> Você deve utilizar somente traço(-), sublinhado(_), ponto(.), dois pontos(:), letras(a-z) e números(0-9).<br>Seu nome de usuário também deve conter de 3 à 15 caracteres.
                                    <div class="text-center">
                                        <img class="img-fluid m-b-10" src="<?=base_url("assets/images/habbo/this-habbo.png")?>" />
                                    </div>
                                    </p>
                                    <div class="fb-button">
                                        <a id="btn-facebook" href="#" class="btn btn-facebook btn-block m-t-5" onclick="fb_login()"><i class="icofont icofont-social-facebook"></i>Criar conta com Facebook</a>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

<?php
$this->load->view('templates/footer');
?>