<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipe extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data["rank_dev"] = $this->players->load_equipe(8);
        $data["rank_ceo"] = $this->players->load_equipe(7);
        $data["rank_adm"] = $this->players->load_equipe(6);
        $data["rank_ger"] = $this->players->load_equipe(5);
        $data["rank_mod"] = $this->players->load_equipe(4);

        $data['titulo'] = 'Mania Hotel: Equipe Staff';
        $this->load->view('equipe', $data);
    }
}
