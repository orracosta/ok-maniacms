<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('News_model', 'news');
    }

    public function index()
    {
        $data["cms_news"] = $this->news->get_news(10);
        $data['last_news'] = $data["cms_news"][0];
        $data['titulo'] = 'Mania Hotel: '.$data["cms_news"][0]["title"];
        $this->load->view('noticia', $data);
    }

    public function view($id)
    {
        if($id != "" AND $id != 0){
            $data["cms_news"] = $this->news->get_news(10);
            $query = $this->news->get_newsid($id);

            if($query) {
                $data['last_news'] = $query;
                $data['titulo'] = 'Mania Hotel: '.$data["last_news"]["title"];
            } else {
                $data['last_news'] = $data["cms_news"][0];
                $data['titulo'] = 'Mania Hotel: '.$data["cms_news"][0]["title"];
            }
            $this->load->view('noticia', $data);
        }else {
            $data["cms_news"] = $this->news->get_news(10);
            $data['last_news'] = $data["cms_news"][0];
            $data['titulo'] = 'Mania Hotel: '.$data["cms_news"][0]["title"];
            $this->load->view('noticia', $data);
        }
    }
}
