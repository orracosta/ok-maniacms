<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel extends CI_Controller {

    private function checkSession(){
        if(!$this->session->has_userdata('loggedIn'))
            redirect(base_url());
        elseif($this->session->has_userdata('loggedIn') AND $this->session->userdata('username') == "[facebook]")
            redirect(base_url("registro/usuario"));
    }

    public function index()
    {
        $this->checkSession();
        $player_id = $this->session->userdata('player_id');
        $query = $this->db->get('cms_client');
        $data["client"] = $query->row_array();
        $data["player"] = $player_id;

        $ticket  = criarsso($player_id);
        $this->players->updatesso($ticket, $player_id);

        $data['ticket'] = $ticket;
        $data['titulo'] = 'Mania Hotel: Bom jogo & divirta-se!';
        $this->load->view('client', $data);
    }
}
