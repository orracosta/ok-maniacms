<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro extends CI_Controller {

    private function checkSession(){
        if($this->session->has_userdata('loggedIn') AND $this->session->userdata('username') == "[facebook]")
            redirect(base_url("registro/usuario"));
    }

    public function index()
    {
        $this->checkSession();
        $data['titulo'] = 'Mania Hotel: Registre-se!';
        $this->load->view('registro', $data);
    }

    public function usuario()
    {
        if($this->session->has_userdata('loggedIn') AND $this->session->userdata('username') == "[facebook]") {
            $data['titulo'] = 'Mania Hotel: Escolha seu nome de usuário!';
            $this->load->view('players/nameuser', $data);
        }else {
            redirect(base_url());
        }
    }

    public function checkemail($email = "", $json = 1)
    {
        //if isn't JSON, set $email.
        if($this->input->post('email')){
            $email = $this->input->post('email');
        }

        //check if is a valid email.
        $result = $this->players->checkemail($email);
        if ($result OR fc_checkemail($email)) {
            $response = array('valid' => 'false');
        } else {
            $response = array('valid' => 'true');
        }

        return jreturn($response, $json);
    }

    public function checkname($username = "", $json = 1)
    {
        //if isn't JSON, set $username.
        if($this->input->post('username')){
            $username = $this->input->post('username');
        }

        //Check if is a valid username.
        $result = $this->players->checkname($username);
        if ($result OR fc_checkusername($username)) {
            $response = array('valid' => 'false');
        } else {
            $response = array('valid' => 'true');
        }

        return jreturn($response, $json);
    }

    public function checkpassword($pass = "", $json = 1)
    {
        //SIf isn't JSON, set $pass.
        if($this->input->post('password')){
            $pass = $this->input->post('password');
        }

        //Check if is a valid pass.
        if (fc_checkpass($pass)) {
            $response = array('valid' => 'false');
        } else {
            $response = array('valid' => 'true');
        }

        return jreturn($response, $json);
    }

    public function registrar()
    {
        $this->checkSession();
        if($this->input->post() AND $this->input->post('g-recaptcha-response')){

            //Check method like curl, checking the result of captcha
            $resposta = googlecaptcha($this->input->post('g-recaptcha-response'));

            //if all is good, execute.
            if($resposta["success"]){
                //Set variables
                $username = $this->input->post('username');
                $email = $this->input->post('email');
                $passwd = $this->input->post('senha');
                $passwdTwo = $this->input->post('senhaTwo');

                //Store checks like array
                $check = array(
                    $this->checkemail($email, 0),
                    $this->checkpassword($passwd, 0),
                    $this->checkname($username, 0),
                );
                //Do all checks. If's good, insert.
                if(fc_checkalltrue($check) == "true" AND
                    fc_checkequal($passwd, $passwdTwo)){
                    $this->players->registeruser();
                    $result = $this->players->getbyname($username);

                    foreach ($result as $row)
                    {
                        $array = array(
                            'player_id' => $row->id,
                            'loggedIn' => TRUE
                        );
                    }
                    $this->players->insertpsettings($array['player_id']);
                    $this->session->set_userdata($array);

                    if($this->session->has_userdata('refuser'))
                        $this->session->set_userdata('registro');

                    return redirect(base_url());
                }
                else
                    return redirect(base_url('registro'));
            }
        } else {
            return redirect(base_url('registro'));
        }
    }

    public function regusuario()
    {
        if($this->input->post()){
            //Set variables
            $player_id = $this->session->userdata('player_id');
            $username = $this->input->post('username');

            //Store checks like array
            $check = array(
                $this->checkname($username, 0),
            );
            //Do all checks. If's good, insert.
            if(fc_checkalltrue($check) == "true"){
                $this->players->updateusername($player_id, $username);

                $this->session->unset_userdata('username');
                return redirect(base_url());
            }
            else
                return redirect(base_url('registro/usuario'));
        } else {
            return redirect(base_url('registro/usuario'));
        }
    }
}
