<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    private function checkSession(){
        if($this->session->has_userdata('loggedIn') AND $this->session->userdata('username') == "[facebook]")
            redirect(base_url("registro/usuario"));
        $this->load->model('News_model', 'news');
        $this->load->model('Cms_model', 'cms');
    }

    public function index()
    {
        $this->checkSession();

        //Give players of hall and hot news to view.
        $cms_status = $this->cms->server_status();
        $data["hall_semanal"] = $this->players->load_hall('s_points', 3);
        $data["hall_mensal"] = $this->players->load_hall('m_points', 3);
        $data["hall_promo"] = $this->players->load_hall('promo_pts', 3);
        $data["hall_ref"] = $this->players->load_hall('ref_points', 3);
        $data["cms_news"] = $this->news->get_news(5);
        $data["cms_status"] = $cms_status[0];

        $data['titulo'] = 'Mania Hotel: Crie seu Habbo, Construa seu quarto, Converse e Faça Novos Amigos!';
        if($this->session->has_userdata('loggedIn')){
            $data["player"] = $this->players->getbyid($this->session->userdata('player_id'));
            $data['titulo'] = 'Mania Hotel: Bem-vindo(a) '.$data["player"]["username"].'!';
        }

        $this->load->view('home', $data);
    }

    public function login()
    {
        $this->checkSession();
        $response = array('valid'=>'false');

        //Check if post exist.
        if($this->input->post("action")){
            $username = $this->input->post('username');
            $password = fc_criptpass($this->input->post('password'));

            $result = $this->players->checklogin($username, $password);
            if ($result) {
                $response = array('valid' => 'true');

                foreach ($result as $row)
                {
                    $array = array(
                        'player_id' => $row->id,
                        'username' => $row->username,
                        'loggedIn' => TRUE
                    );
                }
                $this->session->set_userdata($array);
            }
        }
        echo json_encode($response);
    }

    public function facebooklogin(){
        $this->checkSession();
        $response = array('valid'=>'false');

        //Check if post exist
        if($this->input->post("action")){
            $response = 403;

            $oauth_id = $this->input->post("id");
            $email = $this->input->post("email");

            //Check if facebook user is already registered.
            $result = $this->players->facebookcheck($oauth_id);
            if ($result) {
                foreach ($result as $row)
                {
                    $array = array(
                        'player_id' => $row->id,
                        'username' => $row->username,
                        'loggedIn' => TRUE
                    );
                }
                $this->session->set_userdata($array);
                $response = 201;
            } else {
                //If not, register like new user.
                $array = array(
                    'oauth_id' => $oauth_id,
                    'email' => $email
                );

                $this->players->regfacebook($array);
                $result = $this->players->facebookcheck($oauth_id);
                foreach ($result as $row)
                {
                    $array = array(
                        'player_id' => $row->id,
                        'username' => $row->username,
                        'loggedIn' => TRUE
                    );
                }
                $this->players->insertpsettings($array['player_id']);
                $this->session->set_userdata($array);
                $response = 200;

                if($this->session->has_userdata('refuser'))
                    $this->session->set_userdata('registro');
            }
        }
        echo json_encode($response);
    }

    public function setref($username)
    {
        $result = $this->players->getbyname($username);
        if($result){
            foreach ($result as $row)
            {
                $array = array(
                    'refuser' => $row->id,
                );

                $this->session->set_userdata($array);
                redirect(base_url('registro'));
            }
        }else {
            redirect(base_url());
        }
    }

    public function logout()
    {
        session_destroy();
        redirect(base_url());
    }
}
