<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ranking extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data["hall_semanal"] = $this->players->load_hall('s_points', 10);
        $data["hall_mensal"] = $this->players->load_hall('m_points', 10);
        $data["hall_global"] = $this->players->load_hall('g_points', 10);
        $data["hall_promo"] = $this->players->load_hall('promo_pts', 10);

        $data['titulo'] = 'Mania Hotel: Hall da Fama';
        $this->load->view('ranking', $data);
    }
}
