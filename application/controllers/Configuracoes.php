<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracoes extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    private function checkSession(){
        if(!$this->session->has_userdata('loggedIn'))
            redirect(base_url());
    }

    public function index()
    {
        $this->checkSession();
        $data["player"] = $this->players->getbyid($this->session->userdata('player_id'));

        $data['titulo'] = 'Mania Hotel: Configurações';
        $this->load->view('configuracoes', $data);
    }

    public function senhaantiga($password = "", $json = 1)
    {
        $this->checkSession();

        //If isn't json, set $password.
        if($this->input->post('password')){
            $password = fc_criptpass($this->input->post('password'));
        }

        //Check old password. If true, do it!
        $result = $this->players->checkoldpass($this->session->userdata('player_id'), $password);
        if ($result) {
            $response = array('valid' => 'true');
        } else {
            $response = array('valid' => 'false');
        }

        return jreturn($response, $json);
    }

    public function mudarsenha($json = 1)
    {
        $this->checkSession();

        $password = fc_criptpass($this->input->post('senhantiga'));
        $passwordNew = fc_criptpass($this->input->post('password'));

        //Check old password. If true, do it!
        $result = $this->players->checkoldpass($this->session->userdata('player_id'), $password);
        if ($result) {
            $this->players->updatePass($this->session->userdata('player_id'), $passwordNew);
            $response = array('valid' => 'true');
        } else {
            $response = array('valid' => 'false');
        }

        return jreturn($response, $json);
    }

}
