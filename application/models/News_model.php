<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    public function get_news($limit){
        return $this->db
            ->order_by('id', 'DESC')
            ->limit($limit)
            ->get('cms_news')
            ->result_array();
    }
    public function get_newsid($id)
    {
        return $this->db
            ->get_where('cms_news', array('id' => $id))
            ->row_array();
    }
}
