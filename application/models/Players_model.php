<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Players_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    public function checkemail($email){
        return $this->db
            ->where('email', $email)
            ->from('players')
            ->count_all_results();
    }
    public function checkname($username){
        return $this->db
            ->where('username', $username)
            ->from('players')
            ->count_all_results();
    }
    public function registeruser(){
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $senha = $this->input->post('senha');
        $ip = cloudflareip();

        $senha = fc_criptpass($senha);

        if($this->session->has_userdata('refuser')) {
            $this->db->insert('players', $data = array(
                'username' => $username,
                'password' => $senha,
                'email' => $email,
                'refid' => $this->session->userdata('refuser'),
                'last_ip' => $ip,
                'reg_timestamp' => time(),
                'reg_date' => date("d/m/Y")
            ));
        }else{
            $this->db->insert('players', $data = array(
                'username' => $username,
                'password' => $senha,
                'email' => $email,
                'last_ip' => $ip,
                'reg_timestamp' => time(),
                'reg_date' => date("d/m/Y")
            ));
        }
    }
    public function regfacebook($array){
        $username = "[facebook]";
        $email = $array['email'];
        $oauth_id = $array['oauth_id'];
        $ip = cloudflareip();

        $senha = fc_criptpass(rand(999999,9999999)."mania2018");

        if($this->session->has_userdata('refuser')) {
            $this->db->insert('players', $data = array(
                'username' => $username,
                'oauth_id' => $oauth_id,
                'password' => $senha,
                'refid' => $this->session->userdata('refuser'),
                'email' => $email,
                'last_ip' => $ip,
                'reg_timestamp' => time(),
                'reg_date' => date("d/m/Y")
            ));
        }else{
            $this->db->insert('players', $data = array(
                'username' => $username,
                'oauth_id' => $oauth_id,
                'password' => $senha,
                'email' => $email,
                'last_ip' => $ip,
                'reg_timestamp' => time(),
                'reg_date' => date("d/m/Y")
            ));
        }


    }
    public function checklogin($username, $password){
        return $this->db
            ->get_where('players', array('username' => $username, 'password' => $password))
            ->result();
    }
    public function facebookcheck($oauth){
        return $this->db
            ->get_where('players', array('oauth_id' => $oauth))
            ->result();
    }
    public function getbyname($username){
        return $this->db
            ->get_where('players', array('username' => $username))
            ->result();
    }
    public function getbyid($id){
        return $this->db
            ->get_where('players', array('id' => $id))
            ->row_array();
    }
    public function updateusername($id, $username){

        $this->db->set('username', $username);
        $this->db->where('id', $id);
        $this->db->update('players');
    }
    public function updatesso($sso, $playerid){
        $this->db->set('auth_ticket', $sso);
        $this->db->where('id', $playerid);
        $this->db->update('players');
    }
    public function insertpsettings($id){
        $this->db->insert('player_settings', $data = array(
            'player_id' => $id
        ));
    }
    public function load_hall($id, $limit){
        return $this->db
            ->order_by($id, 'DESC')
            ->limit($limit)
            ->get_where('players', array('rank' => 1))
            ->result_array();
    }
    public function load_equipe($rank){
        return $this->db
            ->get_where('players', array('rank' => $rank))
            ->result_array();
    }
    public function checkoldpass($id, $password){
        return $this->db
            ->get_where('players', array('id' => $id, 'password' => $password))
            ->result();
    }
    public function updatePass($id, $password){
        $this->db->set('password', $password);
        $this->db->where('id', $id);
        $this->db->update('players');
    }
}
