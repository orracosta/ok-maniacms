<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    public function server_status(){
        return $this->db
            ->limit(1)
            ->get('server_status')
            ->result_array();
    }
}
